﻿using CargoWise.eHub.Common;
using CargoWise.eHub.Common.Extensions;
using System.IO;
using System.ServiceModel;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using Chilkat;
using System;

namespace CargoWise.eAdaptorSampleWebService
{
	public class eAdaptorSampleWebService : IeAdapterStreamedService
	{
        SqlConnection conn = new SqlConnection("server=MEMPHIS4; database=edi; uid=SecureAdmin; pwd=wc$f@J?ayw;");
        string destinationFolder = @"E:\eAdaptorXml";
        string _log_file = "";
		string _metric_log_file = @"E:\eAdaptorXml\logs\runtime_metrics.log";

        public bool Ping()
		{
			return true;
		}

		public void SendStream(SendStreamRequest request)
		{
			writeToMetricLog(@"request_start," + (DateTime.Now.Ticks / TimeSpan.TicksPerMillisecond).ToString());
            //===run a check to see if there exists a log file
            //   for the current month...
			writeToMetricLog(@"chklog_start," + (DateTime.Now.Ticks / TimeSpan.TicksPerMillisecond).ToString());
            checkLogFiles();
			writeToMetricLog(@"chklog_end," + (DateTime.Now.Ticks / TimeSpan.TicksPerMillisecond).ToString());
			
            writeToLog(@"== " + DateTime.Now.ToString() + " ==");

            string senderId = OperationContext.Current.ServiceSecurityContext.PrimaryIdentity.Name;

			writeToMetricLog(@"validatefolder_start," + (DateTime.Now.Ticks / TimeSpan.TicksPerMillisecond).ToString());
			Validate(destinationFolder);
			writeToMetricLog(@"validatefolder_end," + (DateTime.Now.Ticks / TimeSpan.TicksPerMillisecond).ToString());
			
            foreach (var message in request.Messages)
			{
				writeToMetricLog(@"processxml_start," + (DateTime.Now.Ticks / TimeSpan.TicksPerMillisecond).ToString());
                string fileName = string.Format("Message_{1}_{0}.xml", message.MessageTrackingID, senderId);

                writeToLog(@"" + message.MessageTrackingID.ToString() + " | " + senderId + "");

                // Save to a file on the file system
                writeToMetricLog(@"storefile_start," + (DateTime.Now.Ticks / TimeSpan.TicksPerMillisecond).ToString());
                using (var fileStream = new FileStream(Path.Combine(destinationFolder, fileName), FileMode.Create))
				{
					message.MessageStream.DecodeAndDecompress().WriteTo(fileStream);
				}
                writeToMetricLog(@"storefile_end," + (DateTime.Now.Ticks / TimeSpan.TicksPerMillisecond).ToString());

                // Store in the database for processin
                string pathToFile = destinationFolder + @"\" + fileName;

                try
                {
                    ProcessXmlFile(pathToFile, message.MessageTrackingID, senderId);

                    // Delete the original file we created from the eAdaptor message
                    File.Delete(pathToFile);
                }
                catch (Exception ex) {
                    writeToLog(@"ERROR in file" + pathToFile + " : " + ex.Message);

                }
                //finally
                //{
                //    // Delete the original file we created from the eAdaptor message
                //    File.Delete(pathToFile);
                //}                

                //string xml;

                //using (var streamReader = new StreamReader(pathToFile))
                //{
                //    xml = streamReader.ReadToEnd();
                //}

                //QueueToSQL(xml.Replace("utf-8","utf-16"));
				writeToMetricLog(@"processxml_end," + (DateTime.Now.Ticks / TimeSpan.TicksPerMillisecond).ToString());
            }
			writeToMetricLog(@"request_end," + (DateTime.Now.Ticks / TimeSpan.TicksPerMillisecond).ToString());
		}

        public void ProcessXmlFile(string XmlFilePath, Guid TrackingId, string SenderId)
        {
            writeToMetricLog(@"ripxml_start," + (DateTime.Now.Ticks / TimeSpan.TicksPerMillisecond).ToString());
            // Create a GUID to use in naming our files.
            Guid g;
            g = Guid.NewGuid();

            // Load the target file in our "MainXml" variable
            // Console.WriteLine("Initializing XML variables");
            Chilkat.Xml MainXml = new Chilkat.Xml();
            Chilkat.Xml EntryXml = new Chilkat.Xml();
            Chilkat.Xml CommercialXml = new Chilkat.Xml();

            bool success;
            success = MainXml.LoadXmlFile(XmlFilePath);
            if (success != true)
            {
                //Console.WriteLine(MainXml.LastErrorText);
                return;
            }

            // Determine the XML document type we are dealing with
            string XmlType = "";

            if (MainXml.HasChildWithTag("Body|UniversalShipment"))
                XmlType = "UniversalShipment";
            if (MainXml.HasChildWithTag("Body|UniversalEvent"))
                XmlType = "UniversalEvent";
            if (MainXml.HasChildWithTag("Body|Native"))
                XmlType = "Native";

            // Console.WriteLine("=================================================");
            // Console.WriteLine("=================================================");
            // Console.WriteLine(XmlType);
            // Console.WriteLine("=================================================");
            // Console.WriteLine("=================================================");

            if (XmlType == "UniversalShipment")
            {
                // 1) EntryXml
                // Console.WriteLine("Populating EntryXml");
                EntryXml = MainXml.GetChildWithTag("Body|UniversalShipment|Shipment|EntryHeaderCollection");

                // 2) CommercialXml
                // Console.WriteLine("Populating CommercialXml");
                CommercialXml = MainXml.GetChildWithTag("Body|UniversalShipment|Shipment|CommercialInfo");

                // 3) MainXml
                // ==== Remove the CommercialInfo node and all child content
                // Console.WriteLine("Buildinmg MainXml");
                MainXml.RemoveChild("Body|UniversalShipment|Shipment|CommercialInfo");
                MainXml.RemoveChild("Body|UniversalShipment|Shipment|EntryHeaderCollection");
            }
            writeToMetricLog(@"ripxml_end," + (DateTime.Now.Ticks / TimeSpan.TicksPerMillisecond).ToString());

            // ==== No matter what type of XML we are working with, we need to push it to
            //      the database. If it happened to be UniversalShipmentXml, then it will have
            //      received some massaging before getting to this point. Otherwise, the XML will
            //      will be submitted, as-is.
            // Console.WriteLine("Writing to Database");
            // Write xml to DB
            writeToMetricLog(@"tosql_start," + (DateTime.Now.Ticks / TimeSpan.TicksPerMillisecond).ToString());
            QueueToSQL(MainXml.GetXml().Replace("utf-8", "utf-16"), TrackingId, SenderId);
            writeToMetricLog(@"tosql_end," + (DateTime.Now.Ticks / TimeSpan.TicksPerMillisecond).ToString());

            // if this WAS UniversalShipment XML, then we have some files we want to write. However,
            // we did not want to delay writing to the database before we did this, thus why we are 
            // doing it now.

            writeToMetricLog(@"saveaddlxml_start," + (DateTime.Now.Ticks / TimeSpan.TicksPerMillisecond).ToString());
            if (XmlType == "UniversalShipment")
            {
                // Console.WriteLine("Outputting to XML files");
                // Save EntryXml
                // Console.WriteLine("...EntryXml...");
                if (EntryXml != null)
                    EntryXml.SaveXml(destinationFolder + @"\queue\" + g.ToString() + "_Entry.xml");
                // Save CommercialXml
                // Console.WriteLine("...CommercialXml...");
                if (CommercialXml != null)
                    CommercialXml.SaveXml(destinationFolder + @"\queue\" + g.ToString() + "_Commercial.xml");
                // Save MainXml
                // Console.WriteLine("...MainXml...");
                if (MainXml != null)
                    MainXml.SaveXml(destinationFolder + @"\processed\" + g.ToString() + "_Main.xml");
            }
            writeToMetricLog(@"saveaddlxml_end," + (DateTime.Now.Ticks / TimeSpan.TicksPerMillisecond).ToString());
        }

        public void QueueToSQL(string xml, Guid TrackingId, string SenderId) {
            try
            {
                conn.Open();

                SqlCommand cmd = new SqlCommand("dbo.QueueXML", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@arg_XML", SqlDbType.Xml).Value = xml;
                cmd.Parameters.Add("@arg_MessageTrackingId", SqlDbType.UniqueIdentifier).Value = TrackingId;
                cmd.Parameters.Add("@arg_SenderId", SqlDbType.VarChar, 10).Value = SenderId;
                cmd.ExecuteNonQuery();

                conn.Close();

                writeToLog(@"SUCCESS!");
            }
            catch (Exception ex) {
                writeToLog(@"ERROR: " + ex.Message);
                conn.Close();
            }                      
        }

		public void Validate(string destinationFolder)
		{
			if ((destinationFolder == "")) throw new FaultException("No destination folder for received messages has been specified. Please modify the eAdaptorSampleWebService.cs file located within the eAdaptor solution and specify a destination folder for received messages.");
		}

        //===check to see if the log file for the current month/year exists. If
        //   not, then create a new log file. We are going to create a new log
        //   file every month...
        public void checkLogFiles()
        {
            string logfilename = "" + destinationFolder + @"\logs\" + "ProcessLog_" + DateTime.Now.Month.ToString() + DateTime.Now.Year.ToString() + ".log";

            if (!File.Exists(logfilename))
            {
                //===create the new file...
                File.WriteAllText(logfilename, "");
            }

            _log_file = logfilename;
        }

        //===write an entry to the current log file...
        public void writeToLog(string argLogText)
        {
            using (StreamWriter sw = File.AppendText(_log_file))
            {
                sw.WriteLine(argLogText);
            }
        }
		
		//===write an entry to the current log file...
        public void writeToMetricLog(string argLogText)
        {
            using (StreamWriter sw = File.AppendText(_metric_log_file))
            {
                sw.WriteLine(argLogText);
            }
        }
    }
}
